def serverPort = 9999
def imageVersion = 'latest'
def registryUrl = '127.0.0.1:5000'
def projectName = 'wol'
pipeline {
    agent any

    options {
        disableConcurrentBuilds()
        ansiColor('xterm')
    }

    stages {
        stage('Pull latest image from registry') {
            steps {
                sh "docker pull ${registryUrl}/${projectName}:${imageVersion}"
            }
        }

        stage('Remove Container') {
            steps {
                script {
                    try {
                        sh "docker rm -f ${projectName}-container || true"
                    } catch (exception) {
                        echo "${exception}"
                    }
                }
            }
        }

        stage('Deployment') {
            steps {
                sh "docker run -d \
                    --env-file=${HOME}/.docker/env/ssh.env \
                    -e SERVER_PORT=${serverPort} \
                    -p ${serverPort}:${serverPort} \
                    --restart always --name \
                    ${projectName}-container \
                    -t ${registryUrl}/${projectName}:${imageVersion}"
            }
        }

        stage('Docker cleanup') {
            steps {
                sh "docker container prune -f"
                sh "docker image prune -f"
            }
        }
    }

    post {
        always {
            deleteDir()
        }

        failure {
            mail to: "${env.TEAM_MAIL_LIST}",
                    subject: "Failure :: ${currentBuild.fullDisplayName}",
                    body: "Failed deployment: ${env.BUILD_URL}"
        }

        success {
            mail to: "${env.TEAM_MAIL_LIST}",
                 subject: "Successful deployment for ${projectName} :: ${currentBuild.fullDisplayName}",
                 body: "Deployment finished successfully: ${env.BUILD_URL}"
        }

    }

}
