FROM raspbian:java8-lite
RUN apt-get update && apt-get install -y iputils-ping && apt-get install -y wakeonlan
CMD bash
MAINTAINER Youssef Benkaryouh
ARG version=1.0.0-SNAPSHOT
ADD target/wol-${version}.jar /opt/wol.jar
ENTRYPOINT ["java", "-jar", "/opt/wol.jar"]