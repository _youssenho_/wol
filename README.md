# Wol Overview

Wake-on-lan A.K.A WOL is an Ethernet or Token Ring computer networking standard that allows a computer to be turned on or awakened by a network message.

The message is usually sent to the target computer by a program executed on a device connected to the same local area network. 

It is also possible to initiate the message from another network by using subnet directed broadcasts or a WoL gateway service.

# About this application

This application run run in Windows, Unix and Linux OS. A useful way to benefit is to deploy it in a raspberry, witch is a low power consumption device, and then exporting the application to be visible from outside your network.

Once you turn on your device using this application you can access it remotely using a third party application such team viewer or any other.

![WOL-UI](https://bitbucket.org/youssenho/wol/raw/6dddaeda3379483aab8b8d9eab1c23128e20bf6f/src/main/resources/templates/images/wol-ui.JPG)

## Running locally
Before running the application update application.yml with target computer information

```
my-computer:
  name: DESKTOP
  ip: 192.168.0.100
  port: 9
  mac-address: 70:8B:CD:9E:D7:27
```

and then:

``` 
mvn spring-boot:run 
```

# Road map

- Support cloud config
- Support more security implementations
- Support multi devices controlling
- Integration with hm-manager to get the device information

