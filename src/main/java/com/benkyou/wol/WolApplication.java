package com.benkyou.wol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class}, scanBasePackages = "com.benkyou.wol.*")
public class WolApplication {

    public static void main(String[] args) {
        SpringApplication.run(WolApplication.class, args);
    }

}


