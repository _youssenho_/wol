package com.benkyou.wol.command;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import static com.benkyou.wol.util.IOUtils.readBufferedReader;

@Slf4j
@Component
public class CommandLineRunner {

    private Runtime runtime;

    public CommandLineRunner() {
        runtime = Runtime.getRuntime();
    }

    public String runCommand(String command, CharSequence delimiter) {
        StringBuilder output = new StringBuilder();
        try {
            Process process = runtime.exec(command);
            output.append(bufferedReaderAsString(process.getInputStream(), delimiter));
            output.append(bufferedReaderAsString(process.getErrorStream(), delimiter));
        } catch (IOException e) {
            log.error("exception happened - here's what I know: {}", e.getMessage());
            output.append(e.getMessage());
            output.append(delimiter);
            log.trace("error :", e);
        }
        return output.toString();
    }

    private String bufferedReaderAsString(InputStream inputStream, CharSequence delimiter) throws IOException {
        StringBuilder sb = new StringBuilder();
        Optional<String> std = Optional.ofNullable(readBufferedReader(Optional.ofNullable(inputStream)));
        std.ifPresent(value -> {
            log.error(std.get());
            sb.append(std.get());
            sb.append(delimiter);
        });
        return sb.toString();
    }

}
