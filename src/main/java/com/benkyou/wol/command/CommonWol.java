package com.benkyou.wol.command;

import com.benkyou.wol.model.Device;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class CommonWol implements Wol {

    private static final String HTML_BR = "<br/>";

    protected CommandLineRunner commandLineRunner;

    public CommonWol(CommandLineRunner commandLineRunner) {
        this.commandLineRunner = commandLineRunner;
    }

    @Override
    public String switchOn(Device device, CharSequence delimiter) {
        return commandLineRunner.runCommand(getSwitchOnCommand(device), delimiter);
    }

    protected abstract String getSwitchOnCommand(Device device);

    public boolean isUp(String ip, CharSequence delimiter) {
        String commandResult = commandLineRunner.runCommand(getPingCommand(ip), delimiter);
        return !(commandResult.contains("Unreachable") || commandResult.contains("inaccesible"));
    }

    protected abstract String getPingCommand(String ip);

    // TODO pending to be implemented
    @Override
    public String switchOff(Device device, CharSequence delimiter) {
        return "Not implemented yet! " + delimiter;
    }

}
