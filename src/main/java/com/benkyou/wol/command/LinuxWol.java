package com.benkyou.wol.command;

import com.benkyou.wol.config.os.LinuxEnvironmentCondition;
import com.benkyou.wol.model.Device;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Component;

@Component
@Conditional(LinuxEnvironmentCondition.class)
public class LinuxWol extends CommonWol {

    public LinuxWol(CommandLineRunner commandLineRunner) {
        super(commandLineRunner);
    }

    @Override
    protected String getSwitchOnCommand(Device device) {
        return String.format("wakeonlan %s", device.getMacAddress());
    }

    @Override
    protected String getPingCommand(String ip) {
        return String.format("ping %s -c 4", ip);
    }

}
