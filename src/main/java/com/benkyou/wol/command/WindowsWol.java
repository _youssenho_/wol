package com.benkyou.wol.command;

import com.benkyou.wol.config.os.WindowsEnvironmentCondition;
import com.benkyou.wol.model.Device;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Component;

@Component
@Conditional(WindowsEnvironmentCondition.class)
public class WindowsWol extends CommonWol {

    public WindowsWol(CommandLineRunner commandLineRunner) {
        super(commandLineRunner);
    }

    @Override
    protected String getSwitchOnCommand(Device device) {
        return String.format("WolCmd.exe %s %s 255.255.255.255 %d",
                device.getMacAddress().replace(":", ""),
                device.getIp(),
                device.getWolPort());
    }

    @Override
    protected String getPingCommand(String ip) {
        return String.format("ping %s", ip);
    }

}
