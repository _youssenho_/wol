package com.benkyou.wol.command;

import com.benkyou.wol.model.Device;

public interface Wol {

    String switchOn(Device device, CharSequence delimiter);

    String switchOff(Device device, CharSequence delimiter);

    boolean isUp(String ip, CharSequence delimiter);

}
