package com.benkyou.wol.config.os;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.net.URL;

@Slf4j
@Configuration
@Conditional(WindowsEnvironmentCondition.class)
public class WindowsConfig {

    @PostConstruct
    public void setupWolCmdLib() throws IOException {
        copyWolCmdFile();
    }

    private void copyWolCmdFile() throws IOException {
        URL inputUrl = getClass().getResource("/WolCmd.exe");
        File destinationFile = new File("WolCmd.exe");
        FileUtils.copyURLToFile(inputUrl, destinationFile);
        log.info("Wol command copied successfully to: " + destinationFile.getAbsoluteFile().getAbsolutePath());
    }

}
