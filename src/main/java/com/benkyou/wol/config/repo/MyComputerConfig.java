package com.benkyou.wol.config.repo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Slf4j
@Configuration
@ConfigurationProperties(prefix = "my-computer")
@ConditionalOnProperty(prefix = "toggle", name = "device-config-repo-enabled", havingValue = "true", matchIfMissing = true)
public class MyComputerConfig {

    private String ip;
    private int port;
    private String macAddress;

}
