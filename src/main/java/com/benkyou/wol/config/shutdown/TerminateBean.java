package com.benkyou.wol.config.shutdown;

import lombok.extern.slf4j.Slf4j;

import javax.annotation.PreDestroy;

@Slf4j
public class TerminateBean {

    @PreDestroy
    public void onDestroy() throws Exception {
        log.error("Sorry to tell you that this application is not compatible with your OS");
    }

}
