package com.benkyou.wol.controller;

import com.benkyou.wol.model.ResponseModel;
import com.benkyou.wol.service.WolService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.function.Function;

@Slf4j
@RestController
@AllArgsConstructor
public class UiController {

    private WolService wolService;

    @PostMapping("/start-desktop")
    @ResponseBody
    public ResponseEntity<ResponseModel> startDesktop() {
        return runWolServiceCommand("Start desktop command", function -> wolService.start());
    }

    @PostMapping("/stop-desktop")
    public @ResponseBody
    ResponseEntity<ResponseModel> stopDesktop() {
        return runWolServiceCommand("Stop desktop command", function -> wolService.stop());
    }

    @GetMapping("/check-desktop-status")
    @ResponseBody
    public ResponseEntity<ResponseModel> checkDesktop() {
        return runWolServiceCommand("Desktop status command", function -> wolService.status());
    }

    private ResponseEntity<ResponseModel> runWolServiceCommand(String infoMessage, Function<Void, ResponseModel> function) {
        log.info("Command received : {}", infoMessage);
        ResponseModel response = function.apply(null);
        log.info("{}: {} ", infoMessage, response.getDescription());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/login")
    public String login(Principal principal) {
        return principal != null && ((Authentication) principal).isAuthenticated() ?
                "forward:/" :
                "login";
    }

}
