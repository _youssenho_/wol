package com.benkyou.wol.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import static com.benkyou.wol.service.CommonWolService.DELIMITER_KEY;

@Getter
@AllArgsConstructor
public class Device {

    private String macAddress;
    private String ip;
    private String name;
    private int wolPort;
    @Setter
    private String status;

    @Override
    public String toString() {
        return String.join(DELIMITER_KEY,
                String.format("Computer name: %s", this.name),
                String.format("Mac address: %s", this.macAddress),
                String.format("ip: %s", this.ip),
                String.format("WOL port: %d", this.wolPort),
                String.format("Status: %s", this.status));
    }

}