package com.benkyou.wol.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class ResponseModel {

    private Object response;
    private String type;
    private String description;

}
