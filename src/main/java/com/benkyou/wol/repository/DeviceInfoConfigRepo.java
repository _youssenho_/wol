package com.benkyou.wol.repository;

import com.benkyou.wol.config.repo.MyComputerConfig;
import com.benkyou.wol.model.Device;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

@Repository
@AllArgsConstructor
@ConditionalOnProperty(prefix = "toggle", name = "device-config-repo-enabled", havingValue = "true", matchIfMissing = true)
public class DeviceInfoConfigRepo implements DeviceInfoRepo {

    private MyComputerConfig computerConfig;

    @Override
    public Device findById(String deviceName) {
        return new Device(computerConfig.getMacAddress(),
                computerConfig.getIp(),
                deviceName,
                computerConfig.getPort(),
                null);
    }

}
