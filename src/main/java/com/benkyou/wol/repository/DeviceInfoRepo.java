package com.benkyou.wol.repository;

import com.benkyou.wol.model.Device;

public interface DeviceInfoRepo {

    Device findById(String deviceName);

}
