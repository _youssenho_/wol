package com.benkyou.wol.service;

import com.benkyou.wol.command.Wol;
import com.benkyou.wol.repository.DeviceInfoRepo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnProperty(prefix = "toggle", name = "cli-wol-enabled", havingValue = "true")
public class CliWolService extends CommonWolService {

    protected CliWolService(Wol wol, DeviceInfoRepo deviceInfoRepo, @Value("${my-computer.name}") String myComputerName) {
        super(wol, deviceInfoRepo, myComputerName);
    }

    @Override
    protected CharSequence delimiter() {
        return "\n";
    }

}
