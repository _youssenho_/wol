package com.benkyou.wol.service;

import com.benkyou.wol.command.Wol;
import com.benkyou.wol.model.Device;
import com.benkyou.wol.model.ResponseModel;
import com.benkyou.wol.repository.DeviceInfoRepo;

public abstract class CommonWolService implements WolService {

    public static final String DELIMITER_KEY = "@DELIMITER@";

    private Wol wol;
    private final Device myComputer;

    protected CommonWolService(Wol wol, DeviceInfoRepo deviceInfoRepo, String myComputerName) {
        this.wol = wol;
        this.myComputer = deviceInfoRepo.findById(myComputerName);
    }

    public ResponseModel start() {
        if (wol.isUp(myComputer.getIp(), delimiter())) {
            return buildResponseModel("The computer is already running");
        } else {
           return buildResponseModel(wol.switchOn(myComputer, delimiter()));
        }
    }

    public ResponseModel stop() {
        if (!wol.isUp(myComputer.getIp(), delimiter())) {
            return buildResponseModel("The computer seems to be off");
        } else {
            return buildResponseModel(wol.switchOff(myComputer, delimiter()));
        }
    }

    public ResponseModel status() {
        myComputer.setStatus(wol.isUp(myComputer.getIp(), delimiter()) ? "ON" : "OFF");
        return buildResponseModel(myComputer.toString().replace(DELIMITER_KEY, delimiter()));
    }

    private ResponseModel buildResponseModel(String description) {
        return ResponseModel
                .builder()
                .response(myComputer)
                .type("info")
                .description(description)
                .build();
    }

    protected abstract CharSequence delimiter();

}
