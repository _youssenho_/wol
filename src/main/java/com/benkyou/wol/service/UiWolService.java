package com.benkyou.wol.service;

import com.benkyou.wol.command.Wol;
import com.benkyou.wol.repository.DeviceInfoRepo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class UiWolService extends CommonWolService {

    protected UiWolService(Wol wol, DeviceInfoRepo deviceInfoRepo, @Value("${my-computer.name}") String myComputerName) {
        super(wol, deviceInfoRepo, myComputerName);
    }

    @Override
    protected CharSequence delimiter() {
        return "\t";
    }

}
