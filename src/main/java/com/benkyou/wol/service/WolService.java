package com.benkyou.wol.service;

import com.benkyou.wol.model.ResponseModel;
import org.springframework.stereotype.Service;

@Service
public interface WolService {

    ResponseModel start();

    ResponseModel stop();

    ResponseModel status();

}
