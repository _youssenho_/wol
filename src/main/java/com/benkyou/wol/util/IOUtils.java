package com.benkyou.wol.util;

import lombok.experimental.UtilityClass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Optional;
import java.util.stream.Collectors;

@UtilityClass
public class IOUtils {

    public static String readBufferedReader(Optional<InputStream> inputStream) throws IOException {
        if (inputStream.isPresent()) {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream.get()))) {
                return readAllLinesWithStream(reader);
            }
        }
        return null;
    }

    private static String readAllLinesWithStream(BufferedReader reader) {
        return reader
                .lines()
                .collect(Collectors.joining(System.lineSeparator()));
    }

}
