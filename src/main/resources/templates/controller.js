var app = angular.module('console', ['ui.bootstrap']);
app.controller('controller', function($scope, $http, $interval) {

    $scope.desktopStatusAvailable = false;
    $scope.waitingResponse = false;
    $scope.alert = {
        type: 'form-group alert alert-light',
        message: ''
    }

    $scope.start = function() {
        $scope.postAction('start-desktop');
    }

    $scope.stop = function() {
        $scope.postAction('stop-desktop');
    }

    $scope.postAction = function(path) {
            $scope.waitingResponse = true;
            $scope.desktopStatusAvailable = false;
            $scope.setAlert('info', 'Submitting, please wait...');
            $http.post(path).success(function(data) {
                $scope.waitingResponse = false;
                $scope.setAlert('success', data);
            }).error(function(data) {
                $scope.waitingResponse = false;
                $scope.setAlert('danger', data.message);
            });
    }

    $scope.getStatus = function() {
            $scope.waitingResponse = true;
            $scope.desktopStatusAvailable = false;
            $scope.setAlert('info', 'Getting status, please wait...');
            $http.get('check-desktop-status').success(function(data) {
               $scope.desktopStatusAvailable = true;
               $scope.setAlert('success', data.response);
               $scope.waitingResponse = false;
            }).error(function(data) {
               $scope.setAlert('danger', data.message);
               $scope.waitingResponse = false;
            });
    }

    $scope.logout = function() {
            $scope.desktopStatusAvailable = false;
            $scope.setAlert('info', 'Logging out, please wait...');
            $http.post('logout').success(function(data) {
                $scope.setAlert('success', 'Logged out successfully');
            }).error(function(data) {
                $scope.setAlert('danger', data.message);
            });
    }

    $scope.setAlert = function(type, msg) {
        $scope.alert = {
            type: 'form-group alert alert-' + type,
            message: msg
        }
    }
});