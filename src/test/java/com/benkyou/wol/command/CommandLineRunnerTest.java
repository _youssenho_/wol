package com.benkyou.wol.command;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CommandLineRunnerTest {

    private CommandLineRunner commandLineRunner;
    private Runtime runtime;
    private Process process;

    @BeforeEach
    void beforeTests() {
        commandLineRunner = new CommandLineRunner();
        runtime = mock(Runtime.class);
        process = mock(Process.class);
        ReflectionTestUtils.setField(commandLineRunner, "runtime", runtime);
    }

    @Test
    void GIVEN_ValidInputCommand_WHEN_CommandIsExecuted_THEN_ReturnTheResultInStringFormat() throws IOException {
        //GIVEN
        String command = "java -version";
        CharSequence delimiter = "</br>";

        //WHEN
        String expectedRuntimeReturn = "java version \"11.0.11\" 2021-04-20 LTS";
        when(process.getInputStream()).thenReturn(IOUtils.toInputStream(expectedRuntimeReturn, StandardCharsets.UTF_8));
        when(runtime.exec(anyString())).thenReturn(process);

        //THEN
        assertThat(commandLineRunner.runCommand(command, delimiter)).isEqualTo(expectedRuntimeReturn + delimiter);
    }

    @Test
    void GIVEN_InvalidInputCommand_WHEN_CommandIsExecuted_THEN_ReturnTheResultInStringFormat() throws IOException {
        //GIVEN
        String command = "foo -version";
        CharSequence delimiter = "</br>";

        //WHEN
        when(runtime.exec(anyString())).thenThrow(new IOException("Invalid command"));

        //THEN
        assertThat(commandLineRunner.runCommand(command, delimiter)).isEqualTo("Invalid command" + delimiter);
    }


}
