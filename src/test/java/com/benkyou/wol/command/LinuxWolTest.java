package com.benkyou.wol.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LinuxWolTest extends TestCommonWol {

    @BeforeEach
    public void beforeTests() {
        setup();
        wol = new LinuxWol(commandLineRunner);
    }


    @Test
    void GIVEN_TheApplicationIsRunningInWindows_WHEN_SwitchOnIsCalled_MyComputerIsStarted() {
        WHEN_SwitchedOnIsCalled_THEN_MyComputerIsStarted();
    }

}