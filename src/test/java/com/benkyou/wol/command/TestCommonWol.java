package com.benkyou.wol.command;


import com.benkyou.wol.model.Device;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

abstract class TestCommonWol {

    protected static final String DELIMITER = "<br/>";
    protected Wol wol;
    protected CommandLineRunner commandLineRunner;
    protected Device myComputer;

    protected void setup() {
        commandLineRunner = mock(CommandLineRunner.class);
        when(commandLineRunner.runCommand(any(), anyString())).thenReturn("Success</br>");
        myComputer = new Device("aa:bb:cc:dd:ee:ff", "127.0.0.1", "DESKTOP", 9, null);
    }

    abstract void beforeTests();

    protected void WHEN_SwitchedOnIsCalled_THEN_MyComputerIsStarted() {
        //WHEN
        String result = wol.switchOn(myComputer, DELIMITER);

        //THEN
        assertThat(result).isEqualTo("Success</br>");
        assertThat(wol.isUp(myComputer.getIp(), DELIMITER)).isTrue();
    }

    protected void WHEN_SwitchedOnIsCalled_THEN_MyComputerIsGoingOut() {
        //WHEN
        when(commandLineRunner.runCommand(any(), anyString())).thenReturn("success</br>");
        String result = wol.switchOff(myComputer, DELIMITER);

        //THEN
        assertThat(result).isEqualTo("Not implemented yet! <br/>");
        when(commandLineRunner.runCommand(any(), anyString())).thenReturn("Unreachable");
        assertThat(wol.isUp(myComputer.getIp(), DELIMITER)).isFalse();
    }

}