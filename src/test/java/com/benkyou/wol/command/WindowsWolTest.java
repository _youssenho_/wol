package com.benkyou.wol.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class WindowsWolTest extends TestCommonWol {

    @BeforeEach
    void beforeTests() {
        setup();
        wol = new WindowsWol(commandLineRunner);
    }

    @Test
    void GIVEN_TheApplicationIsRunningInWindows_WHEN_SwitchOnIsCalled_MyComputerIsStarted() {
        WHEN_SwitchedOnIsCalled_THEN_MyComputerIsStarted();
    }

    @Test
    void GIVEN_TheApplicationIsRunningInWindows_WHEN_SwitchOffIsCalled_MyComputerShouldGoOut() {
        WHEN_SwitchedOnIsCalled_THEN_MyComputerIsGoingOut();
    }

}