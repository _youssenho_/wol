package com.benkyou.wol.controller;

import com.benkyou.wol.model.ResponseModel;
import com.benkyou.wol.service.WolService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class UiControllerTest {

    private WolService wolService;
    private UiController uiController;

    @BeforeEach
    void beforeTests() {
        wolService = mock(WolService.class);
        uiController = new UiController(wolService);
    }

    @Test
    void GIVEN_MyComputerIsAlreadyOn_WHEN_StartOrderIsPerformed_THEN_SwitchOnIsNotInvoked() {
        //GIVEN
        when(wolService.start()).thenReturn(ResponseModel.builder().description("The computer is already running").build());

        //WHEN
        ResponseEntity<ResponseModel> result = uiController.startDesktop();

        //THEN
        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        assertThat(result.getBody().getDescription()).isEqualTo("The computer is already running");
    }

    @Test
    void GIVEN_MyComputerIsOff_WHEN_StartOrderIsPerformed_THEN_SwitchOnIsInvoked() {
        //GIVEN
        when(wolService.start()).thenReturn(ResponseModel.builder().description("success").build());

        //WHEN
        ResponseEntity<ResponseModel> result = uiController.startDesktop();

        //THEN
        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        assertThat(result.getBody().getDescription()).isEqualTo("success");
    }

    @Test
    void GIVEN_MyComputerIsOn_WHEN_StopOrderIsPerformed_THEN_SwitchOffIsInvoked() {
        //GIVEN
        when(wolService.stop()).thenReturn(ResponseModel.builder().description("success").build());

        //WHEN
        ResponseEntity<ResponseModel> result = uiController.stopDesktop();

        //THEN
        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        assertThat(result.getBody().getDescription()).isEqualTo("success");
    }

    @Test
    void GIVEN_MyComputerIsAlreadyOff_WHEN_StopOrderIsPerformed_THEN_SwitchOffIsNotInvoked() {
        //GIVEN
        when(wolService.stop()).thenReturn(ResponseModel.builder().description("The computer seems to be off").build());

        //WHEN
        ResponseEntity<ResponseModel> result = uiController.stopDesktop();

        //THEN
        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        assertThat(result.getBody().getDescription()).isEqualTo("The computer seems to be off");
    }

    @Test
    void GIVEN_MyComputerIsOn_WHEN_StatusOrderIsPerformed_THEN_ReturnUP() {
        //GIVEN
        when(wolService.status()).thenReturn(new ResponseModel(null, null, "UP"));

        //WHEN
        ResponseEntity<ResponseModel> result = uiController.checkDesktop();

        //THEN
        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        assertThat(result.getBody().getDescription()).isEqualTo("UP");
    }

    @Test
    void GIVEN_MyComputerIsOff_WHEN_StatusOrderIsPerformed_THEN_ReturnDOWN() {
        //GIVEN
        when(wolService.status()).thenReturn(new ResponseModel(null, null, "DOWN"));

        //WHEN
        ResponseEntity<ResponseModel> result = uiController.checkDesktop();

        //THEN
        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        assertThat(((ResponseModel) result.getBody()).getDescription()).isEqualTo("DOWN");
    }

    @Test
    void GIVEN_AuthenticatedUser_WHEN_LoginIsPerformed_THEN_ForwardToRequestedPage() {
        //GIVEN
        Authentication principal = mock(Authentication.class);
        when(principal.isAuthenticated()).thenReturn(true);

        //WHEN
        String result = uiController.login(principal);

        //THEN
        assertThat(result).isEqualTo("forward:/");
    }

    @Test
    void GIVEN_NonAuthenticatedUser_WHEN_LoginIsPerformed_THEN_ForwardToLoginPage() {
        //GIVEN
        Authentication principal = mock(Authentication.class);
        when(principal.isAuthenticated()).thenReturn(false);

        //WHEN
        String result = uiController.login(principal);

        //THEN
        assertThat(result).isEqualTo("login");
    }

    @Test
    void GIVEN_NullAuthentication_WHEN_LoginIsPerformed_THEN_ForwardToLoginPage() {
        //WHEN
        String result = uiController.login(null);

        //THEN
        assertThat(result).isEqualTo("login");
    }

}