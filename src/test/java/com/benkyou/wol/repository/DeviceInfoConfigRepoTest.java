package com.benkyou.wol.repository;

import com.benkyou.wol.config.repo.MyComputerConfig;
import com.benkyou.wol.model.Device;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class DeviceInfoConfigRepoTest {

    private static final String MY_COMPUTER_NAME = "DESKTOP";

    private MyComputerConfig computerConfig;
    private DeviceInfoRepo deviceInfoRepo;

    @BeforeEach
    void beforeTests() {
        expectedDevice = new Device("aa:bb:cc:dd:ee:ff", "127.0.0.1", MY_COMPUTER_NAME, 9, null);
        computerConfig = new MyComputerConfig();
        computerConfig.setMacAddress(expectedDevice.getMacAddress());
        computerConfig.setIp(expectedDevice.getIp());
        computerConfig.setPort(expectedDevice.getWolPort());
    }

    private Device expectedDevice;

    @Test
    void GIVEN_DeviceInfoConfigRepoIsEnabled_WHEN_FindByIdIsRequested_THEN_ReturnInfoFromConfig() {
        //GIVEN
        deviceInfoRepo = new DeviceInfoConfigRepo(computerConfig);

        //WHEN
        Device myComputer = deviceInfoRepo.findById(MY_COMPUTER_NAME);

        //THEN
        assertThat(myComputer.getMacAddress()).isEqualTo(expectedDevice.getMacAddress());
        assertThat(myComputer.getIp()).isEqualTo(expectedDevice.getIp());
        assertThat(myComputer.getName()).isEqualTo(expectedDevice.getName());
        assertThat(myComputer.getWolPort()).isEqualTo(expectedDevice.getWolPort());
    }

}