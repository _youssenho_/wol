package com.benkyou.wol.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CliWolServiceTest extends TestWolService {

    private static final CharSequence DELIMITER = "\n";

    @BeforeEach
    void beforeTests() {
        setup();
        wolService = new CliWolService(wol, deviceInfoRepo, myComputerName);
    }

    @Test
    void GIVEN_MyComputerIsAlreadyOn_WHEN_StartOrderIsPerformed_THEN_SwitchOnIsNotInvoked() {
        GIVEN_MyComputerIsAlreadyOn_WHEN_StartOrderIsPerformed_THEN_SwitchOnIsNotInvoked(DELIMITER);
    }

    @Test
    void GIVEN_MyComputerIsOff_WHEN_StartOrderIsPerformed_THEN_SwitchOnIsInvoked() {
        GIVEN_MyComputerIsOff_WHEN_StartOrderIsPerformed_THEN_SwitchOnIsInvoked(DELIMITER);
    }

    @Test
    void GIVEN_MyComputerIsOn_WHEN_StopOrderIsPerformed_THEN_SwitchOffIsInvoked() {
        GIVEN_MyComputerIsOn_WHEN_StopOrderIsPerformed_THEN_SwitchOffIsInvoked(DELIMITER);
    }

    @Test
    void GIVEN_MyComputerIsAlreadyOff_WHEN_StopOrderIsPerformed_THEN_SwitchOffIsNotInvoked() {
        GIVEN_MyComputerIsAlreadyOff_WHEN_StopOrderIsPerformed_THEN_SwitchOffIsNotInvoked(DELIMITER);
    }
}