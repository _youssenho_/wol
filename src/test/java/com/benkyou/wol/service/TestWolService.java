package com.benkyou.wol.service;

import com.benkyou.wol.command.Wol;
import com.benkyou.wol.model.Device;
import com.benkyou.wol.model.ResponseModel;
import com.benkyou.wol.repository.DeviceInfoRepo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

abstract class TestWolService {

    protected Wol wol;
    protected String myComputerName;
    protected DeviceInfoRepo deviceInfoRepo;
    protected WolService wolService;

    void setup() {
        wol = mock(Wol.class);
        myComputerName = "DESKTOP";
        deviceInfoRepo = mock(DeviceInfoRepo.class);
        when(deviceInfoRepo.findById(myComputerName)).thenReturn(new Device("aa:bb:cc:dd:ee:ff", "127.0.0.1", myComputerName, 9, null));
    }

    abstract void beforeTests();

    protected void GIVEN_MyComputerIsAlreadyOn_WHEN_StartOrderIsPerformed_THEN_SwitchOnIsNotInvoked(CharSequence delimiter) {
        //GIVEN
        when(wol.isUp(any(), eq(delimiter))).thenReturn(true);

        //WHEN
        ResponseModel result = wolService.start();

        //THEN
        assertThat(result.getDescription()).isEqualTo("The computer is already running");
    }

    protected void GIVEN_MyComputerIsOff_WHEN_StartOrderIsPerformed_THEN_SwitchOnIsInvoked(CharSequence delimiter) {
        //GIVEN
        when(wol.isUp(any(), eq(delimiter))).thenReturn(false);

        //WHEN
        when(wol.switchOn(any(), eq(delimiter))).thenReturn("success");
        ResponseModel result = wolService.start();

        //THEN
        assertThat(result.getDescription()).isEqualTo("success");
    }

    protected void GIVEN_MyComputerIsOn_WHEN_StopOrderIsPerformed_THEN_SwitchOffIsInvoked(CharSequence delimiter) {
        //GIVEN
        when(wol.isUp(any(), eq(delimiter))).thenReturn(true);

        //WHEN
        when(wol.switchOff(any(), eq(delimiter))).thenReturn("success");
        ResponseModel result = wolService.stop();

        //THEN
        assertThat(result.getDescription()).isEqualTo("success");
    }

    protected void GIVEN_MyComputerIsAlreadyOff_WHEN_StopOrderIsPerformed_THEN_SwitchOffIsNotInvoked(CharSequence delimiter) {
        //GIVEN
        when(wol.isUp(any(), eq(delimiter))).thenReturn(false);

        //WHEN
        ResponseModel result = wolService.stop();

        //THEN
        assertThat(result.getDescription()).isEqualTo("The computer seems to be off");
    }

}
